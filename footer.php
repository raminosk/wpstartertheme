<footer class="site-footer">
	<div class="container">
		<div class="row">
			<?php if ( is_active_sidebar( 'footer-widget-1' ) ) : ?>
				<div class="col-md-4">
					<?php dynamic_sidebar( 'footer-widget-1' ); ?>
				</div>
			<?php endif; ?>
			<?php if ( is_active_sidebar( 'footer-widget-2' ) ) : ?>
				<div class="col-md-4">
					<?php dynamic_sidebar( 'footer-widget-2' ); ?>
				</div>
			<?php endif; ?>
			<?php if ( is_active_sidebar( 'footer-widget-3' ) ) : ?>
				<div class="col-md-4">
					<?php dynamic_sidebar( 'footer-widget-3' ); ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
</footer><!--/.site-footer-->

<div class="container">
	<div class="row">
		<div class="col-md-6">
			<?php get_template_part( 'template-parts/footer/site', 'info' ); ?>
		</div>

		<div class="col-md-6 text-lg-right">
			<?php get_template_part( 'template-parts/footer/site', 'credit' ); ?>
		</div>
	</div>
</div>

<?php wp_footer(); ?>

</body>
</html>
