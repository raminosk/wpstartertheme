<?php

/*
 *
 * Basic setup
 *
 * Change WPStarterTheme to Theme name, with lowercase !!!!
 *
 * Change main to Parent theme name, with lowercase !!!!!
 *
 * Remove this comment !!!!
 *
 *
*/

$hash = '#01';

// ---
// Basic setup
function WPStarterThemeSetup() {

	global $hash;

	add_theme_support( 'title-tag' );

	add_theme_support( 'post-thumbnails' );

	// Custom logo.
	add_theme_support( 'custom-logo', array(
		'height'      => 60,
		'width'       => 270,
		'flex-height' => true,
	));

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );

	// Post formats
	add_theme_support( 'post-formats', array(
		'image', 'video', 'quote', 'link', 'gallery'
	));

	$headerImage = array(
		'width'         => 1600,
		'height'        => 800,
		'default-image' => get_stylesheet_directory_uri() . '/build/img/header.jpg',
		'uploads'       => true,
	);

	add_theme_support( 'custom-header', $headerImage );

	//add_image_size( 'WPStarterThemeThumb', 640, 331, array( 'center', 'center'), true); // TRUE - hard crop

	// Menu
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'WPStarterTheme' ),
	) );

	// Remove main theme setup
	remove_action( 'widgets_init', 'twentyseventeen_widgets_init' );
	remove_action( 'after_setup_theme', 'main_setup' );
	remove_action( 'wp_enqueue_scripts', 'main_scripts' );
	
	
}

add_action( 'after_setup_theme', 'WPStarterThemeSetup' );
// /Basic setup
// ---

// ---
// Assets
function WPStarterThemeStyles() {

	global $hash;

	wp_enqueue_style( 'child-style',
		get_stylesheet_directory_uri() . '/build/css/styles.css',
		null,
		$hash
	);

	wp_enqueue_script( 'jquery',
		get_stylesheet_directory_uri() . '/build/js/jquery.min.js', // path to script
		null,  // Dependecies
		$hash, // Add manual or automatic hash wp_get_theme()->get('Version') for browsers
		true   // Add script to the footer
	);

	wp_enqueue_script( 'tether',
		get_stylesheet_directory_uri() . '/build/js/tether.min.js', // path to script
		null,  // Dependecies
		$hash, // Add manual or automatic hash wp_get_theme()->get('Version') for browsers
		true   // Add script to the footer
	);

	wp_enqueue_script( 'bootstrap',
		get_stylesheet_directory_uri() . '/build/js/bootstrap.min.js', // path to script
		null,  // Dependecies
		$hash, // Add manual or automatic hash wp_get_theme()->get('Version') for browsers
		true   // Add script to the footer
	);


	// Google fonts
	$query_args = array(
		'family' => 'Roboto:300,500',
		'subset' => 'latin-ext'
	);

	wp_register_style( 'google_fonts', add_query_arg( $query_args, "//fonts.googleapis.com/css" ), array(), null );
	wp_enqueue_style('google_fonts');
	
	wp_dequeue_style( 'twenty-twenty-one-style' );
    wp_deregister_style( 'twenty-twenty-one-style' );
    
    wp_dequeue_style( 'twenty-twenty-one-print-style' );
    wp_deregister_style( 'twenty-twenty-one-print-style' );
}

add_action( 'wp_enqueue_scripts', 'WPStarterThemeStyles', 20 );


// /Assets
// ---

// ---
// Include files
//require_once WP_CONTENT_DIR . '/themes/WPStarterTheme/lib/gtm.php';

//require_once WP_CONTENT_DIR . '/themes/WPStarterTheme/lib/dns.php';

require_once WP_CONTENT_DIR . '/themes/wpstartertheme/lib/widgets.php';

//require_once WP_CONTENT_DIR . '/themes/WPStarterTheme/lib/speedup.php';

//require_once WP_CONTENT_DIR . '/themes/WPStarterTheme/lib/shortcodes.php';

//require_once WP_CONTENT_DIR . '/themes/WPStarterTheme/lib/tags.php';

require_once WP_CONTENT_DIR . '/themes/WPStarterTheme/lib/b4walker.php';
// /Include files
// ---

?>
