<?php
	get_header();
?>

<div class="container">
	<?php if ( function_exists('yoast_breadcrumb') && ! is_home() && ! is_front_page() ) { ?>
		<?php yoast_breadcrumb('<div class="breadcrumb">','</div>'); ?>
	<?php } ?>

	<div class="row">
		<div class="col-md-8">
			<?php the_archive_title( '<h1>', '</h1>' ); ?>

			<?php
				$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

				$args = array(
					'paged' => $paged,
					'prev_text' => __('<span class="icon icon-arrow icon-arrow-left"></span>'),
					'next_text' => __('<span class="icon icon-arrow icon-arrow-right"></span>')
				);

				$the_query = new WP_Query( $args );

				// The Loop
				if ($the_query->have_posts ()) {

					while ( $the_query->have_posts () ) {
						$the_query->the_post ();

						get_template_part( 'template-parts/loop/content', 'post' );
					} ?>

					<div class="pagination">
						<?php echo paginate_links( $args ); ?>
					</div><?php
				}
			?>
		</div>
		<div class="col-md-4">
			<?php get_sidebar(); ?>
		</div>
	</div>
</div>


<?php
	get_footer();
?>
