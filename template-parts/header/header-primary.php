<header class="site-header">
	<?php if ( has_nav_menu( 'primary' ) ) : ?>
		<nav class="navbar navbar-expand">
			<div class="container">
				<?php get_template_part( 'template-parts/nav/nav', 'primary' ); ?>
			</div>
		</nav>
	<?php endif; ?>
</header>
