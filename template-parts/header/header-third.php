<header class="site-header">
	<div class="banner" style="background-image: url('<?php header_image(); ?>')">
		<div class="banner-gradient">
			<?php //if ( is_front_page() ) : ?>
				<?php if ( is_active_sidebar( 'widget-banner' ) ) : ?>
					<div class="banner-wrapper-center">
						<?php dynamic_sidebar( 'widget-banner' ); ?>
					</div>
				<?php endif; ?>
			<?php //endif; ?>
		</div>
	</div>

	<?php if ( has_nav_menu( 'primary' ) ) : ?>
		<nav class="navbar navbar-expand">
			<div class="container">
				<?php get_template_part( 'template-parts/nav/nav', 'third' ); ?>
			</div>
		</nav>
	<?php endif; ?>
</header>
