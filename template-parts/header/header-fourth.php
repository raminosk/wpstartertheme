<header class="site-header">
	<div class="banner" style="background-image: url('<?php header_image(); ?>')">
		<div class="container">
			<?php
				if ( has_custom_logo() ) {
					the_custom_logo();
				} else { ?>
					<a class="navbar-brand" href="<?php echo get_home_url(); ?>"><?php echo get_bloginfo('name'); ?></a><?php
				}
			?>
		</div>
	</div>

	<?php if ( has_nav_menu( 'primary' ) ) : ?>
		<nav class="navbar navbar-2 navbar-expand">
			<div class="container">
				<?php get_template_part( 'template-parts/nav/nav', 'fourth' ); ?>

				<div class="navbar-wrapper">
					<div class="navbar-search">
						<?php get_search_form(); ?>
					</div>

					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#bs4navbar" aria-controls="bs4navbar" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon">
							<?php get_template_part('build/img/toggler', 'icon.svg'); ?>
						</span>
					</button>
				</div>
			</div>
		</nav>
	<?php endif; ?>

</header>
