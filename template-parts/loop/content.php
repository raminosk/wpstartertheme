<div class="box">
	<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
		<?php the_post_thumbnail( 'thumbnail', array( 'class' => '' ) ); ?>
	</a>
	<?php the_date('d.m.Y', '<span class="meta">', '</span>'); ?>

	<a href="<?php echo get_permalink(); ?>" title="<?php echo get_the_title(); ?>"><?php echo get_the_title(); ?></a>

	<div class="box-content">
		<?php echo wp_trim_words( get_the_content(), 40, '...' ); ?>
	</div>
</div>
