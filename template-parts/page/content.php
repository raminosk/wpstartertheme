<?php

$perex = get_field('perex');

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<h1><?php echo get_the_title(); ?></h1>

	<div class="post-description">
		<p><strong><?php echo $perex; ?></strong></p>
	</div>

	<?php if ( get_the_post_thumbnail() ) : ?>
		<div class="post-thumbnail">
			<a href="<?php the_permalink(); ?>">
				<?php the_post_thumbnail( 'full' ); ?>
			</a>
		</div><!-- .post-thumbnail -->
	<?php endif; ?>

	<div class="post-content">
		<?php the_content(); ?>
	</div><!-- .entry-content -->

</article><!-- #post-## -->
