<div id="buttons">
		<h1 class="sg-title">
			<a href="#buttons"><span>#</span> Buttons</a>
		</h1>

				<h2>Základné</h2>
				<p>
					<a class="btn btn-primary btn-sm">Základné tlačítko</a>
					<a class="btn btn-primary">Základné tlačítko</a>
					<a class="btn btn-primary btn-lg">Základné tlačítko</a>
					<a class="btn btn-primary-outline">Základné tlačítko</a>
					<a class="btn btn-primary-outline btn-sm">Základné tlačítko</a>
				</p>

<code>
&lt;a class="btn btn-primary btn-sm">Základné tlačítko&lt;/a>
&lt;a class="btn btn-primary">Základné tlačítko&lt;/a>
&lt;a class="btn btn-primary btn-lg">Základné tlačítko&lt;/a>
&lt;a class="btn btn-primary-outline">Základné tlačítko&lt;/a>
&lt;a class="btn btn-primary-outline btn-sm">Základné tlačítko&lt;/a>
</code>

				<p>
					<a class="btn btn-secondary btn-sm">Základné tlačítko</a>
					<a class="btn btn-secondary">Základné tlačítko</a>
					<a class="btn btn-secondary btn-lg">Základné tlačítko</a>
					<a class="btn btn-secondary-outline">Základné tlačítko</a>
					<a class="btn btn-secondary-outline btn-sm">Základné tlačítko</a>
				</p>

<code>
&lt;a class="btn btn-secondary btn-sm">Základné tlačítko&lt;/a>
&lt;a class="btn btn-secondary">Základné tlačítko&lt;/a>
&lt;a class="btn btn-secondary btn-lg">Základné tlačítko&lt;/a>
&lt;a class="btn btn-secondary-outline">Základné tlačítko&lt;/a>
&lt;a class="btn btn-secondary-outline btn-sm">Základné tlačítko&lt;/a>
</code>

				<p>
					<a class="btn btn-danger btn-sm">Červené tlačítko</a>
					<a class="btn btn-danger">Červené tlačítko</a>
					<a class="btn btn-danger btn-lg">Červené tlačítko</a>
					<a class="btn btn-danger-outline">Červené tlačítko</a>
					<a class="btn btn-danger-outline btn-sm">Červené tlačítko</a>
				</p>
<code>
&lt;a class="btn btn-danger btn-sm">Červené tlačítko&lt;/a>
&lt;a class="btn btn-danger">Červené tlačítko&lt;/a>
&lt;a class="btn btn-danger btn-lg">Červené tlačítko&lt;/a>
&lt;a class="btn btn-danger-outline">Červené tlačítko&lt;/a>
&lt;a class="btn btn-danger-outline btn-sm">Červené tlačítko&lt;/a>
</code>

				<p>
					<a class="btn btn-info btn-sm">Modré tlačítko</a>
					<a class="btn btn-info">Modré tlačítko</a>
					<a class="btn btn-info btn-lg">Modré tlačítko</a>
					<a class="btn btn-info-outline">Modré tlačítko</a>
					<a class="btn btn-info-outline btn-sm">Modré tlačítko</a>
				</p>
<code>
&lt;a class="btn btn-info btn-sm">Modré tlačítko&lt;/a>
&lt;a class="btn btn-info">Modré tlačítko&lt;/a>
&lt;a class="btn btn-info btn-lg">Modré tlačítko&lt;/a>
&lt;a class="btn btn-info-outline">Modré tlačítko&lt;/a>
&lt;a class="btn btn-info-outline btn-sm">Modré tlačítko&lt;/a>
</code>

				<p>
					<a class="btn btn-warning btn-sm">Žlté tlačítko</a>
					<a class="btn btn-warning">Žlté tlačítko</a>
					<a class="btn btn-warning btn-lg">Žlté tlačítko</a>
					<a class="btn btn-warning-outline">Žlté tlačítko</a>
					<a class="btn btn-warning-outline btn-sm">Žlté tlačítko</a>
				</p>
<code>
&lt;a class="btn btn-warning btn-sm">Žlté tlačítko&lt;/a>
&lt;a class="btn btn-warning">Žlté tlačítko&lt;/a>
&lt;a class="btn btn-warning btn-lg">Žlté tlačítko&lt;/a>
&lt;a class="btn btn-warning-outline">Žlté tlačítko&lt;/a>
&lt;a class="btn btn-warning-outline btn-sm">Žlté tlačítko&lt;/a>
</code>

				<p>
					<a class="btn btn-success btn-sm">Zelené tlačítko</a>
					<a class="btn btn-success">Zelené tlačítko</a>
					<a class="btn btn-success btn-lg">Zelené tlačítko</a>
					<a class="btn btn-success-outline">Zelené tlačítko</a>
					<a class="btn btn-success-outline btn-sm">Zelené tlačítko</a>
				</p>
<code>
&lt;a class="btn btn-success btn-sm">Zelené tlačítko&lt;/a>
&lt;a class="btn btn-success">Zelené tlačítko&lt;/a>
&lt;a class="btn btn-success btn-lg">Zelené tlačítko&lt;/a>
&lt;a class="btn btn-success-outline">Zelené tlačítko&lt;/a>
&lt;a class="btn btn-success-outline btn-sm">Zelené tlačítko&lt;/a>
</code>

		<div class="row">

			<div class="col-sm-6">
				<h2>Group</h2>
				<div class="btn-group">
					<a href="" class="btn btn-primary mb-10">Primary</a>
					<a href="" class="btn btn-primary mb-10">button</a>
				</div>
				<div class="btn-group">
					<a href="" class="btn btn-danger mb-10">Red</a>
					<a href="" class="btn btn-danger mb-10">button</a>
				</div>
				<div class="btn-group">
					<a href="" class="btn btn-info mb-10">Blue</a>
					<a href="" class="btn btn-info mb-10">button</a>
				</div>
				<div class="btn-group">
					<a href="" class="btn btn-success mb-10">Green</a>
					<a href="" class="btn btn-success mb-10">button</a>
				</div>
				<div class="btn-group">
					<a href="" class="btn btn-warning mb-10">Orange</a>
					<a href="" class="btn btn-warning mb-10">button</a>
				</div>
			</div>
		</div>
	</div><!--/#buttons-->
