<div id="labels">
	<h1 class="sg-title"><a href="#labels" ><span>#</span>Labels</a></h1>

	<div class="row">
		<div class="col-md-6">
			<p class="label label-primary">Základný label</p>
			<p class="label label-secondary">Sekundárny label</p>
			<p class="label label-warning">Upozorňovací label</p>
			<p class="label label-info">Informačný label</p>
			<p class="label label-danger">Chybový label</p>
			<p class="label label-success">Správny label</p>
		</div>
		<div class="col-md-6">
		<div class="wpss-syntax-highlite">
			<code>
			&lt;p class="label label-primary">&lt;/p>
			&lt;p class="label label-secondary">&lt;/p>
			&lt;p class="label label-warning">&lt;/p>
			&lt;p class="label label-info">&lt;/p>
			&lt;p class="label label-danger">&lt;/p>
			&lt;p class="label label-success">&lt;/p>
			</code>
		</div>

		</div>
	</div>
</div><!--/#icons-->
