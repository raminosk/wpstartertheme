<div id="tags">
	<h1 class="sg-title"><a href="#tags" ><span>#</span>Štítky</a></h1>

	<div class="row">
		<div class="col-md-6">
			<p class="tag-link tag-primary">#Základný štítok</p>
			<p class="tag-link tag-secondary">#Sekundárny štítok</p>
			<p class="tag-link tag-warning">#Upozorňovací štítok</p>
			<p class="tag-link tag-info">#Informačný štítok</p>
			<p class="tag-link tag-danger">#Chybový štítok</p>
			<p class="tag-link tag-success">#Správny štítok</p>
		</div>
		<div class="col-md-6">
		<div class="wpss-syntax-highlite">
			<code>
			&lt;p class="tag-link tag-primary">#&lt;/p><br>
			&lt;p class="tag-link tag-secondary">#&lt;/p><br>
			&lt;p class="tag-link tag-warning">#&lt;/p><br>
			&lt;p class="tag-link tag-info">#&lt;/p><br>
			&lt;p class="tag-link tag-danger">#&lt;/p><br>
			&lt;p class="tag-link tag-success">#&lt;/p><br>
			</code>
		</div>

		</div>
	</div>

	<h2>Outline</h2>
	<div class="row">
		<div class="col-md-6">
			<p class="tag-link tag-primary-outline">#Základný štítok</p>
			<p class="tag-link tag-secondary-outline">#Sekundárny štítok</p>
			<p class="tag-link tag-warning-outline">#Upozorňovací štítok</p>
			<p class="tag-link tag-info-outline">#Informačný štítok</p>
			<p class="tag-link tag-danger-outline">#Chybový štítok</p>
			<p class="tag-link tag-success-outline">#Správny štítok</p>
		</div>
		<div class="col-md-6">
		<div class="wpss-syntax-highlite">
			<code>
			&lt;p class="tag-link tag-primary-outline">#&lt;/p><br>
			&lt;p class="tag-link tag-secondary-outline">#&lt;/p><br>
			&lt;p class="tag-link tag-warning-outline">#&lt;/p><br>
			&lt;p class="tag-link tag-info-outline">#&lt;/p><br>
			&lt;p class="tag-link tag-danger-outline">#&lt;/p><br>
			&lt;p class="tag-link tag-success-outline">#&lt;/p><br>
			</code>
		</div>

		</div>
	</div>
</div><!--/#icons-->
