<div id="text">
	<h1 class="sg-title"><a href="#typography"><span>#</span> Typografia</a></h1>

	<h1>Nadpis h1</h1>
	<p>Používaný pri návrhu tlačovín a typografie. Lorem Ipsum je štandardným výplňovým textom už od 16. storočia, keď neznámy tlačiar zobral sadzobnicu plnú tlačových znakov a pomiešal ich, aby tak vytvoril vzorkovú knihu.</p>

	<h2>Nadpis h2</h2>
	<p>Spopularizovaný bol v 60-tych rokoch 20.storočia, vydaním hárkov Letraset, ktoré obsahovali pasáže Lorem Ipsum, a neskôr aj publikačným softvérom ako Aldus PageMaker, ktorý obsahoval verzie Lorem Ipsum.</p>

	<h3>Nadpis h3</h3>
	<p>Je dávno známe, že ak je zrozumiteľný obsah stránky, na ktorej rozloženie sa čitateľ díva, jeho pozornosť je rozptýlená.</p>

	<h4>Nadpis h4</h4>
	<p>Je fakt, že má viacmenej normálne rozloženie písmen, takže oproti použitiu 'Sem príde text, sem príde text' sa obsah vypĺňanej oblasti na stránke viac podobá na skutočný text.</p>

	<h5>Nadpis h5</h5>
	<p>Editorov webových stránok už používajú Lorem Ipsum ako predvolený výplňový text a keď dáte na internete vyhľadávať 'lorem ipsum', objavíte mnoho webových stránok v rannom štádiu ich vzniku.</p>

	<h6>Ukážka zoznamu</h6>

	<ul class="list mb-20">
		<li>Starter šablóna je určená pre štart nového projektu</li>
		<li>Je vhodná pre: </li>
			<ul>
				<li>Blogy</li>
				<li>Firemné stránky</li>
				<li>Osobné prezentácie</li>
			</ul>
		<li>Ponúka viacero farebných variant</li>
		<li>K tomu hosting a dómenu zadarmo</li>
	</ul>
</div><!--/#typography-->
