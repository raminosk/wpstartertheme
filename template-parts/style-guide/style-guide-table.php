<div id="tables">
		<h1 class="sg-title"><a href="#tables" ><span>#</span> Tables</a></h1>
		<div class="row">
			<div class="col-sm-6">
				<table class="table-primary">
					<tr>
						<th>Starter</th>
						<th>Starter plus</th>
						<th>Starter premium</th>
					</tr>
					<tr>
						<td>Farebné varianty</td>
						<td>Farebné varianty alebo<br> Firemná identita</td>
						<td>Farebné varianty alebo<br> Firemná identita</td>
					</tr>
					<tr>
						<td><s>Hosting</s></td>
						<td>Hosting</td>
						<td>Hosting + Doména</td>
					</tr>
					<tr>
						<td><s>24 hodinová podpora</s></td>
						<td>24 hodinová podpora</td>
						<td>24 hodinová podpora</td>
					</tr>
				</table>
			</div>

			<div class="col-sm-6">
				<div class="wpss-syntax-highlite">
					<code>
					&lt;table class="table-primary"&gt;<br>
					&nbsp;&lt;tr&gt;...&lt;/tr&gt;<br>
					&lt;/table&gt;
					</code>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-6">
				<table class="table-primary table-odd">
					<tr>
						<th>Starter</th>
						<th>Starter plus</th>
						<th>Starter premium</th>
					</tr>
					<tr>
						<td>Farebné varianty</td>
						<td>Farebné varianty alebo<br> Firemná identita</td>
						<td>Farebné varianty alebo<br> Firemná identita</td>
					</tr>
					<tr>
						<td><s>Hosting</s></td>
						<td>Hosting</td>
						<td>Hosting + Doména</td>
					</tr>
					<tr>
						<td><s>24 hodinová podpora</s></td>
						<td>24 hodinová podpora</td>
						<td>24 hodinová podpora</td>
					</tr>
				</table>
			</div>

			<div class="col-sm-6">
				<div class="wpss-syntax-highlite">
					<code>
					&lt;table class="table-primary table-odd"&gt;<br>
					&nbsp;&lt;tr&gt;...&lt;/tr&gt;<br>
					&lt;/table&gt;
					</code>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-6">
				<table class="table-primary--light table-hover">
					<tr>
						<th>Starter</th>
						<th>Starter plus</th>
						<th>Starter premium</th>
					</tr>
					<tr>
						<td>Farebné varianty</td>
						<td>Farebné varianty alebo<br> Firemná identita</td>
						<td>Farebné varianty alebo<br> Firemná identita</td>
					</tr>
					<tr>
						<td><s>Hosting</s></td>
						<td>Hosting</td>
						<td>Hosting + Doména</td>
					</tr>
					<tr>
						<td><s>24 hodinová podpora</s></td>
						<td>24 hodinová podpora</td>
						<td>24 hodinová podpora</td>
					</tr>
				</table>
			</div>
			<div class="col-sm-6">
				<div class="wpss-syntax-highlite">
					<code>
					&lt;table class="table-primary--light table-hover"&gt;<br>
					&nbsp;&lt;tr&gt;...&lt;/tr&gt;<br>
					&lt;/table&gt;
					</code>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-6">
				<table class="table-primary--light table-odd">
					<tr>
						<th>Starter</th>
						<th>Starter plus</th>
						<th>Starter premium</th>
					</tr>
					<tr>
						<td>Farebné varianty</td>
						<td>Farebné varianty alebo<br> Firemná identita</td>
						<td>Farebné varianty alebo<br> Firemná identita</td>
					</tr>
					<tr>
						<td><s>Hosting</s></td>
						<td>Hosting</td>
						<td>Hosting + Doména</td>
					</tr>
					<tr>
						<td><s>24 hodinová podpora</s></td>
						<td>24 hodinová podpora</td>
						<td>24 hodinová podpora</td>
					</tr>
				</table>
			</div>
			<div class="col-sm-6">
				<div class="wpss-syntax-highlite">
					<code>
					&lt;table class="table-primary--light table-odd"&gt;<br>
					&nbsp;&lt;tr&gt;...&lt;/tr&gt;<br>
					&lt;/table&gt;
					</code>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-6">
				<table class="table-secondary">
					<tr>
						<th>Starter</th>
						<th>Starter plus</th>
						<th>Starter premium</th>
					</tr>
					<tr>
						<td>Farebné varianty</td>
						<td>Farebné varianty alebo<br> Firemná identita</td>
						<td>Farebné varianty alebo<br> Firemná identita</td>
					</tr>
					<tr>
						<td><s>Hosting</s></td>
						<td>Hosting</td>
						<td>Hosting + Doména</td>
					</tr>
					<tr>
						<td><s>24 hodinová podpora</s></td>
						<td>24 hodinová podpora</td>
						<td>24 hodinová podpora</td>
					</tr>
				</table>
			</div>
			<div class="col-sm-6">
				<div class="wpss-syntax-highlite">
					<code>
					&lt;table class="table-secondary"&gt;<br>
					&nbsp;&lt;tr&gt;...&lt;/tr&gt;<br>
					&lt;/table&gt;
					</code>
				</div>
			</div>
		</div>
	</div><!--/#tables-->
