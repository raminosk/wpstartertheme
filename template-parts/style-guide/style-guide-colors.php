<div id="colors">
	<h1 class="sg-title"><a href="#colors"><span>#</span> Farebná paleta</a></h1>
	<div class="row">
		<div class="col-sm-2 col-lg-4">
			<h2>Základná</h2>
			<span class="color color-primary-light"></span>
			<span class="color color-primary-hover"></span>
			<span class="color color-primary"></span>
			<span class="color color-primary-dark"></span>
		</div>
		<div class="col-sm-2 col-lg-4">
			<h2>Sekundárna</h2>
			<span class="color color-secondary-light"></span>
			<span class="color color-secondary-hover"></span>
			<span class="color color-secondary"></span>
			<span class="color color-secondary-dark"></span>
		</div>
		<div class="col-sm-2 col-lg-4">
			<h2>Úspech</h2>
			<span class="color color-success-light"></span>
			<span class="color color-success-hover"></span>
			<span class="color color-success"></span>
			<span class="color color-success-dark"></span>
		</div>
		<div class="col-sm-2 col-lg-4">
			<h2>Chyba</h2>
			<span class="color color-danger-light"></span>
			<span class="color color-danger-hover"></span>
			<span class="color color-danger"></span>
			<span class="color color-danger-dark"></span>
		</div>
		<div class="col-sm-2 col-lg-4">
			<h2>Upozornenie</h2>
			<span class="color color-warning-light"></span>
			<span class="color color-warning-hover"></span>
			<span class="color color-warning"></span>
			<span class="color color-warning-dark"></span>
		</div>
	</div>
</div>
