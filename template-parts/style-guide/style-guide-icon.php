<div id="icons">
	<h1 class="sg-title"><a href="#icons" ><span>#</span>Icons</a></h1>
	<h2>Social icons</h2>
			<p><a class="icon icon-social icon-facebook"></a></p>
<code>
&lt;p class="icon icon-social icon-facebook">&lt;/p>
</code>

			<p><a class="icon icon-social icon-facebook sm"></a></p>
<code>
&lt;p class="icon icon-social icon-facebook sm">&lt;/p>
</code>

			<p><a class="icon icon-social icon-facebook d-flex align-center-vertical">Facebook</a></p>
<code>
&lt;p class="icon icon-social icon-facebook d-flex align-center-vertical">Facebook&lt;/p>
</code>

	<p><a class="icon icon-social icon-instagram"></a></p>
	<p><a class="icon icon-social icon-twitter"></a></p>

	<p style="display: inline-block; background-color: #000;"><a class="icon icon-social icon-facebook icon-social_light"></a></p>
	<p style="display: inline-block; background-color: #000;"><a class="icon icon-twitter icon-social_light"></a></p>
	<p style="display: inline-block; background-color: #000;"><a class="icon icon-instagram icon-social_light"></a></p>
<code>
&lt;p class="icon icon-social icon-facebook icon-social_light">Facebook&lt;/p>
&lt;p class="icon icon-social icon-twitter icon-social_light">Facebook&lt;/p>
&lt;p class="icon icon-social icon-instagram icon-social_light">Facebook&lt;/p>
</code>

	<h2>Basic icons</h2>

	<p class="icon icon-ok"></p>
<code>
&lt;p class="icon icon-ok">&lt;/p>
</code>

	<p class="icon icon-phone"></p>
<code>
&lt;p class="icon icon-phone">&lt;/p>
</code>

	<p class="icon icon-date"></p>
<code>
&lt;p class="icon icon-date">&lt;/p>
</code>

	<p class="icon icon-email"></p>
<code>
&lt;p class="icon icon-email">&lt;/p>
</code>

	<p class="icon icon-place"></p>
<code>
&lt;p class="icon icon-email">&lt;/p>
</code>

	<p class="icon icon-bank"></p>
<code>
&lt;p class="icon icon-bank">&lt;/p>
</code>

	<p class="icon icon-search"></p>
<code>
&lt;p class="icon icon-search">&lt;/p>
</code>

	<p class="icon icon-comment"></p>
<code>
&lt;p class="icon icon-comment">&lt;/p>
</code>

	<p class="icon icon-like"></p>
<code>
&lt;p class="icon icon-like">&lt;/p>
</code>

	<p class="icon icon-print"></p>
<code>
&lt;p class="icon icon-print">&lt;/p>
</code>


</div><!--/#icons-->
