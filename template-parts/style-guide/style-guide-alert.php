<div id="alerts">
		<h1 class="sg-title"><a href="#alerts" ><span>#</span> Alerts</a></h1>
		<div class="row mb-10">
			<div class="col-sm-6">
				<div class="alert alert-danger">
					<p>Error</p>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="wpss-syntax-highlite">
					<code>
					&lt;div class="alert alert-danger"&gt;<br>
					&nbsp;&lt;p&gt;...&lt;/p&gt;<br>
					&lt;/div&gt;
					</code>
				</div>
			</div>
		</div>

		<div class="row mb-10">
			<div class="col-sm-6">
				<div class="alert alert-warning">
					<p>Warning</p>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="wpss-syntax-highlite">
					<code>
					&lt;div class="alert alert-warning"&gt;<br>
					&nbsp;&lt;p&gt;...&lt;/p&gt;<br>
					&lt;/div&gt;
					</code>
				</div>
			</div>
		</div>

		<div class="row mb-10">
			<div class="col-sm-6">
				<div class="alert alert-info">
					<p>Information</p>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="wpss-syntax-highlite">
					<code>
					&lt;div class="alert alert-info"&gt;<br>
					&nbsp;&lt;p&gt;...&lt;/p&gt;<br>
					&lt;/div&gt;
					</code>
				</div>
			</div>
		</div>

		<div class="row mb-10">
			<div class="col-sm-6">
				<div class="alert alert-success">
					<p>Success</p>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="wpss-syntax-highlite">
					<code>
					&lt;div class="alert alert-success"&gt;<br>
					&nbsp;&lt;p&gt;...&lt;/p&gt;<br>
					&lt;/div&gt;
					</code>
				</div>
			</div>
		</div>

		<div class="row mb-10">
			<div class="col-sm-6">
				<div class="alert alert-ok alert-small">
					<p>Success small</p>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="wpss-syntax-highlite">
					<code>
					&lt;div class="alert alert-ok alert-small"&gt;<br>
					&nbsp;&lt;p&gt;...&lt;/p&gt;<br>
					&lt;/div&gt;
					</code>
				</div>
			</div>
		</div>
	</div><!--/#alerts-->
