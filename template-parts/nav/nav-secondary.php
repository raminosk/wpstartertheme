<?php
	if ( has_custom_logo() ) {
		the_custom_logo();
	} else { ?>
		<a class="navbar-brand" href="<?php echo get_home_url(); ?>"><?php echo get_bloginfo('name'); ?></a><?php
	}
?>
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#bs4navbar" aria-controls="bs4navbar" aria-expanded="false" aria-label="Toggle navigation">
	<span class="navbar-toggler-icon">
		<?php get_template_part('build/img/toggler', 'icon.svg'); ?>
	</span>
</button>
<?php
	wp_nav_menu([
	'menu'            => 'primary',
	'theme_location'  => 'primary',
	'container'       => 'div',
	'container_id'    => 'bs4navbar',
	'container_class' => 'collapse navbar-collapse',
	'menu_id'         => false,
	'menu_class'      => 'navbar-nav mr-auto',
	'depth'           => 2,
	'fallback_cb'     => 'bs4navwalker::fallback',
	'walker'          => new bs4navwalker()
	]);
?>
