<?php
	wp_nav_menu([
	'menu'            => 'primary',
	'theme_location'  => 'primary',
	'container'       => 'div',
	'container_id'    => 'bs4navbar',
	'container_class' => 'collapse navbar-collapse',
	'menu_id'         => false,
	'menu_class'      => 'navbar-nav mr-auto',
	'depth'           => 2,
	'fallback_cb'     => 'bs4navwalker::fallback',
	'walker'          => new bs4navwalker()
	]);
?>
