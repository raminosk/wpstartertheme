<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div class="site">
	<h1>Header #1</h1>
	<?php //get_template_part( 'template-parts/header/header', 'primary' ); ?>

	<h1>Header #2</h1>
	<?php //get_template_part( 'template-parts/header/header', 'secondary' ); ?>

	<h1>Header #3</h1>
	<?php get_template_part( 'template-parts/header/header', 'third' ); ?>

	<h1>Header #4</h1>
	<?php get_template_part( 'template-parts/header/header', 'fourth' ); ?>

	<h1>Header vertical</h1>
	<?php get_template_part( 'template-parts/header/header', 'vertical' ); ?>

