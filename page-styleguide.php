<?php

/*
 * Template name: Styleguide
 *
 *
 */

get_header('header-primary');

?>

<style type="text/css">
	code {
		white-space: pre-wrap;
		display: block;
		margin-bottom: 20px;
	}

	.btn {
		vertical-align: top;
	}
</style>

<div class="container">
	<h1>Style guide</h1>
	<p>Style guide je štartovací bod, vždy začni s prípravou základných prvkov. Neskôr ich budeš v projekte využívať. Style guide, môže slúžiť aj ako ukážka pre klienta.</p>

	<?php get_template_part( 'template-parts/style-guide/style-guide', 'menu' ); ?>

	<?php get_template_part( 'template-parts/style-guide/style-guide', 'colors' ); ?>

	<?php get_template_part( 'template-parts/style-guide/style-guide', 'typo' ); ?>

	<?php get_template_part( 'template-parts/style-guide/style-guide', 'btn' ); ?>

	<?php get_template_part( 'template-parts/style-guide/style-guide', 'labels' ); ?>

	<?php get_template_part( 'template-parts/style-guide/style-guide', 'tags' ); ?>

	<?php get_template_part( 'template-parts/style-guide/style-guide', 'table' ); ?>

	<?php get_template_part( 'template-parts/style-guide/style-guide', 'alert' ); ?>

	<?php get_template_part( 'template-parts/style-guide/style-guide', 'icon' ); ?>

</div>
	<div class="container">
		<h1>Modules</h1>
	</div>

	<div class="container">
		<h1>Header #1</h1>
	</div>

	<?php get_template_part( 'template-parts/header/header', 'primary' ); ?>

	<div class="container">
<code>
&lt;header class="site-header">
	&lt;?php if ( has_nav_menu( 'primary' ) ) : ?>
		&lt;nav class="navbar navbar-expand">
			&lt;div class="container">
				&lt;?php get_template_part( 'template-parts/nav/nav', 'primary' ); ?>
			&lt;/div>
		&lt;/nav>
	&lt;?php endif; ?>
&lt;/header>
</code>
	</div>

	<div class="container">
		<h1>Header #2</h1>
	</div>

	<?php get_template_part( 'template-parts/header/header', 'secondary' ); ?>
	<div class="container">
<code>
&lt;header class="site-header">
	&lt;div class="banner" style="background-image: url('<?php header_image(); ?>')">
		&lt;div class="banner-gradient">
			&lt;?php if ( has_nav_menu( 'primary' ) ) : ?>
				&lt;nav class="navbar navbar-expand">
					&lt;div class="container">
						&lt;?php get_template_part( 'template-parts/nav/nav', 'primary' ); ?>
					&lt;/div>
				&lt;/nav>
			&lt;?php endif; ?>
			&lt;?php if ( is_front_page() ) : ?>
				&lt;?php if ( is_active_sidebar( 'widget-banner' ) ) : ?>
					&lt;div class="banner-wrapper-center">
						&lt;?php dynamic_sidebar( 'widget-banner' ); ?>
					&lt;/div>
				&lt;?php endif; ?>
			&lt;?php endif; ?>
		&lt;/div>
	&lt;/div>
&lt;/header>
</code>
	</div>

	<div class="container">
		<h1>Header #3</h1>
	</div>
	<?php get_template_part( 'template-parts/header/header', 'third' ); ?>
	<div class="container">
<code>
&lt;header class="site-header">
	&lt;div class="banner" style="background-image: url('<?php header_image(); ?>')">
		&lt;div class="banner-gradient">
			&lt;?php if ( is_front_page() ) : ?>
				&lt;?php if ( is_active_sidebar( 'widget-banner' ) ) : ?>
					&lt;div class="banner-wrapper-center">
						&lt;?php dynamic_sidebar( 'widget-banner' ); ?>
					&lt;/div>
				&lt;?php endif; ?>
			&lt;?php endif; ?>
		&lt;/div>
	&lt;/div>

	&lt;?php if ( has_nav_menu( 'primary' ) ) : ?>
		&lt;nav class="navbar navbar-expand">
			&lt;div class="container">
				&lt;?php get_template_part( 'template-parts/nav/nav', 'third' ); ?>
			&lt;/div>
		&lt;/nav>
	&lt;?php endif; ?>
&lt;/header>
</code>
	</div>

	<h1>Header #4</h1>
	<?php get_template_part( 'template-parts/header/header', 'fourth' ); ?>

	<h1>Header vertical</h1>
	<?php get_template_part( 'template-parts/header/header', 'vertical' ); ?>

</div>

<?php
get_footer();
?>
