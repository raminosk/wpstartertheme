<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="site-content">

	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<h1 class="page-title"><?php _e( 'Oops! That page can&rsquo;t be found.', 'twentyseventeen' ); ?></h1>

				<div class="content">

					<p><?php _e( 'It looks like nothing was found at this location. Maybe try a search?', 'twentyseventeen' ); ?></p>

					<?php get_search_form(); ?>

				</div><!--/.content -->
			</div>
			<div class="col-md-4">
				<?php get_sidebar(); ?>
			</div>
		</div><!--/.row-->
	</div><!--/.container-->
</div><!--/.site-content-->

<?php get_footer();

