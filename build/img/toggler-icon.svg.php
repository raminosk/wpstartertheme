<svg width="35px" height="22px" viewBox="0 0 35 22" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <!-- Generator: Sketch 49 (51002) - http://www.bohemiancoding.com/sketch -->
    <desc>Created with Sketch.</desc>
    <defs></defs>
    <g id="Welcome" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="Desktop-HP" transform="translate(-455.000000, -37.000000)" fill="#000">
            <rect id="Rectangle-7" x="455" y="37" width="35" height="2" rx="1"></rect>
            <rect id="Rectangle-7" x="455" y="47" width="35" height="2" rx="1"></rect>
            <rect id="Rectangle-7" x="455" y="57" width="35" height="2" rx="1"></rect>
        </g>
    </g>
</svg>