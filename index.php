<?php
	get_header();
?>

	<div class="hero center">
		<div class="container">
			<h1>WPStarterTheme</h1>
		</div>
	</div><!--/.hero-->
	
	<div class="container">
		<h1>Title</h1>
		<p>It is a long established fact that a <a href="">Základný odkaz</a> reader will be distracted <a href="" class="link-danger">Červený odkaz</a> by the readable content of a page when looking at its layout. The point of using <a href="" class="link-info">Modrý odkaz</a> Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page <a href="" class="link-success">Zelený odkaz</a> editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>
		
		<p><a href="" class="btn btn-primary mb-10">Základné tlačítko</a></p>
		<p><a href="" class="btn btn-danger mb-10">Červené tlačítko</a></p>
		<p><a href="" class="btn btn-info mb-10">Modré tlačítko</a></p>
		<p><a href="" class="btn btn-success mb-10">Zelené tlačítko</a></p>
		<p><a href="" class="btn btn-warning mb-10">Oranžové tlačítko</a></p>
		
		<h2>Title 2</h2>
		<ul class="mb-10">
									<li>Starter šablóna je určená pre štart nového projektu</li>
									<li>Je vhodná pre: </li>
										<ul>
											<li>Blogy</li>
											<li>Firemné stránky</li>
											<li>Osobné prezentácie</li>
										</ul>
									<li>Ponúka viacero farebných variant</li>
									<li>K tomu hosting a dómenu zadarmo</li>
								</ul>
	</div>

	<?php //get_template_part( 'page', 'styleguide' ); ?>

	<?php
		// WP_Query arguments
		$args = array(
			'category_name'	=> 'novinky',
			'posts_per_page'	=> '5',
		);

		$query = new WP_Query ( $args );

		// The Loop
		if ($query->have_posts ()) {
			while ( $query->have_posts () ) {
				$query->the_post ();

				get_template_part( 'template-parts/loop/content', get_post_format() );
			}
		}
	?>
</div>


<?php
	get_footer();
?>
