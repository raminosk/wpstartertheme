<?php
/*
 @url: https://www.isaumya.com/how-to-enable-dns-prefetching-in-wordpress/
 */

function xxxDNS() {
echo '<meta http-equiv="x-dns-prefetch-control" content="on">
<link rel="dns-prefetch" href="//fonts.googleapis.com" />';
}

add_action('wp_head', 'xxxDNS', 0);