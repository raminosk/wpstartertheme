<?php

function getCurrentCatID() {
	if ( is_category() ) {
		$category = get_category( get_query_var('cat') );

		$catID = $category->cat_ID;

		return $catID;

	} elseif ( is_single() ) {
		$category = get_the_category( get_query_var('cat') );

		$catID = $category[0]->term_id;

		return $catID;
	}
}

function getCurrentCatSlug() {
	if ( is_category() ) {
		$category = get_category( get_query_var('cat') );

		$catID = $category->cat_ID;
	} else {
		$category = get_the_category( get_query_var('cat') );

		$catID = $category[0]->term_id;
	}

	return $catID;
}

function get_cat_slug($cat_id) {
	$cat_id = (int) $cat_id;
	$category = get_category($cat_id);
	return $category->slug;
}

function getIDFromCatName($slug) {

	$catSlug = get_category_by_slug($slug);
	$catName = $catSlug->cat_ID;

	return $catName;
}

function getParentCatID() {
	$catParentID = 0;

	if ( is_category( ) ) {
		$category = get_category( get_query_var('cat') );

		$catParentID = $category->parent;
	}

	return $catParentID;
}

function getCurrentTagID() {
	$tag_id = get_queried_object()->term_id;

	return $tag_id;
}

/*function getACFFromCat($acfValue, $cat) {
	$catValue = get_field($acfValue, 'category_'. getCurrentCatID());

	return $catValue;
}*/

