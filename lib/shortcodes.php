<?php

// ---
// Post shortcode
add_shortcode('productID', 'evlysProductID');

function evlysProductID($atts, $content){

	extract(shortcode_atts(array( // a few default values
		'posts_per_page' => '1')
	, $atts));

	global $post;

	$posts = new WP_Query($atts);

	$output = '';
	if ($posts->have_posts())
		while ($posts->have_posts()):
			$posts->the_post();

			$bannerDescription = get_field('banner-description', $post->ID);

			$postDescriptionInfobox = get_field('post-description', $post->ID);

			$large_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );

			$out = '<div class="banner">
						<div class="container">
							<div class="banner-wrapper">
								<h3 class="banner-title">'.get_the_title() .'</h3>
								<p class="banner-subtitle">'.$bannerDescription.'</p>
							</div>
							<div class="banner-img"><img src="'.$large_image[0].'"></div>
						</div>
					</div>';
			$out .='<div class="container">
						<div class="row">
							<div class="col-md-6">
								<div class="infobox infobox-elvys infobox-indent">
									<p>'.$postDescriptionInfobox.'</p>
								</div>
							</div>
						</div>
					</div>';
					/* these arguments will be available from inside $content
						get_permalink()
						get_the_content()
						get_the_category_list(', ')
						get_the_title()
						and custom fields
						get_post_meta($post->ID, 'field_name', true);
					*/
		endwhile;
	else
		return; // no posts found

	wp_reset_query();

	return html_entity_decode($out);
}
// [productID p=postID]
// /Post shortcode
// ---
