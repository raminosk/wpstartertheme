<?php

/*
 * SpeedUP WordPress setup
 *
 * Change xxx to Theme name, with lowercase
 * Remove this comment
 *
*/

// ---
// Disable emoji
function xxxDisableEmoji() {
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
	add_filter( 'tiny_mce_plugins', 'disableEmojisTinymce' );
	add_filter( 'wp_resource_hints', 'disableEmojiDNSprefetch', 10, 2 );
}

add_action( 'init', 'xxxDisableEmoji' );

function disableEmojisTinymce( $plugins ) {
	if ( is_array( $plugins ) ) {
		return array_diff( $plugins, array( 'wpemoji' ) );
	} else {
		return array();
	}
}


function disableEmojiDNSprefetch( $urls, $relation_type ) {
	if ( 'dns-prefetch' == $relation_type ) {
		$emoji_svg_url = apply_filters( 'emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/' );
		$urls = array_diff( $urls, array( $emoji_svg_url ) );
 	}

 	return $urls;
}
// /Disable emoji
// ---

// ---
// Remove RSD, WordPress version and Manifest links
remove_action( 'wp_head', 'rsd_link' );

remove_action( 'wp_head', 'wlwmanifest_link' );

remove_action( 'wp_head', 'wp_generator' );
// /Remove RSD and Manifest links
// ---

// ---
// Remove JQuery migrate
function xxxRemoveScripts( $scripts ) {
	if ( ! is_admin() && isset( $scripts->registered['jquery'] ) ) {
		$script = $scripts->registered['jquery'];

	if ( $script->deps ) { // Check whether the script has any dependencies
			$script->deps = array_diff( $script->deps, array( 'jquery-migrate' ) );
		}
	}
}

add_action( 'wp_default_scripts', 'xxxRemoveScripts' );
// /Remove JQuery migrate
// ---

// ---
// Remove unused image sizes from twentyseventeen theme
function remove_plugin_image_sizes() {
	remove_image_size('twentyseventeen-featured-image');
	remove_image_size('twentyseventeen-thumbnail-avatar');
}

add_action('init', 'remove_plugin_image_sizes');
// /Remove unused image sizes from twentyseventeen theme
// ---

