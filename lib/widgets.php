<?php

/*
 * Basic setup
 *
 * Change xxx to Theme name, with lowercase
 * Remove this comment
 *
*/

function wpstWidgets() {
	register_sidebar( array(
		'name'          => __( 'Banner description', 'xxx' ),
		'id'            => 'widget-banner',
		'description'   => __( 'Add widgets here to appear in your sidebar on blog posts and archive pages.', 'xxx' ),
		'before_widget' => '<div class="banner-description">',
		'after_widget'  => '</div>',
		'before_title'  => '<h1 class="banner-title">',
		'after_title'   => '</h1>',
	) );
}
add_action( 'widgets_init', 'wpstWidgets' );
